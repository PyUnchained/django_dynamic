from random import randint
from django.test import TestCase
from models import DynamicModel, DynamicEavConfig

from eav.models import Attribute
import eav

eav.register(DynamicModel, DynamicEavConfig)

# Create your tests here.
class ModelsTestCase(TestCase):

	def test_dynamic_model(self):
		dm = DynamicModel()
		dm.save()
		dm2 = DynamicModel()
		dm2.save()

		for attr in ['text','float','int','date','boolean','object']:
			dm.add_field('first_%s' % str(randint(1,1000000000)), attr)
			dm2.add_field('second_%s' % str(randint(1,100000000)), attr)
		
		dm.save()



