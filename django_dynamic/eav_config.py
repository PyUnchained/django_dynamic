import eav
from app.models import Patient, Encounter

eav.register(Encounter)
eav.register(Patient)