from __future__ import unicode_literals
from random import randint

from django.db import models
from django.db.models.query_utils import Q
import eav
from eav.models import Attribute
from eav.registry import EavConfig

FIELD_TYPES = [('text', u'text'),('float', u'float'),('int', u'integer'),('date', u'date'),
	('boolean', u'Yes/No'), ('object', u'Foreign Key')]

# Create your models here.
class DynamicModel(models.Model):


	def add_field(self, name, datatype, scope = 'object'):
		"""Add a field to all instances of this specific model class."""

		attr_string = 'TYPE_'+ datatype.strip().upper()
		datatype = getattr(Attribute, attr_string)

		#Determine the type to set, based on the scope
		type_str = self.__class__.__name__
		Attribute.objects.get_or_create(name=name, datatype=datatype,
			type = type_str)
		print 'New Attr'

	def remove_field(self, name):
		"""Remove a field from all instances of this specific model class."""

		fields = Attribute.objects.filter(name = name,
			type = self.__class__.__name__)
		for f in fields:
			f.delete()

class DynamicEavConfig(EavConfig):

    @classmethod
    def get_attributes(cls, model):
    	query = []
    	if hasattr(model, 'base_class'):
    		attributes = model.base_class.attributes
    		# q = Q(type = model.base_class.__class__.__name__)
    		# q |= Q(type = model.__class__.__name__)
    		# query = Attribute.objects.filter(q)

    		return attributes | query
    	else:
    		q = Q(type = model.__class__.__name__)

        return Attribute.objects.all()

