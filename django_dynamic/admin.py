from django.contrib import admin

from models import *
from eav.forms import BaseDynamicEntityForm
from eav.admin import BaseEntityAdmin


class DynamicModelAdminForm(BaseDynamicEntityForm):
    model = DynamicModel

class DynamicModelAdmin(BaseEntityAdmin):
    form = DynamicModelAdminForm

admin.site.register(DynamicModel, DynamicModelAdmin)